Ant Movie Catalog source code
(c) 2000-2017 Antoine Potten, Micka�l Vanneufville

http://www.antp.be/software/moviecatalog/


CONTENTS
========

In this package (amc_sources.zip) you find four folders :

- Movie Catalog : it contains the program source code, under GPL licence. See licence.txt for more information.
- Common : some files that uses shared code, used in some other programs that I made. Most are GPL or MPL. It must be in a "Common" folder sibling to the folder where Ant Renamer source code is located.
- FreeReport : modified version of latest FreeReport ( http://freereport.sourceforge.net ) to which I made some changes.
- IFPS : modified version of Innerfuse Pascal Script v2 ( http://www.carlo-kok.com/ifps.php ) which is no more available, and were I modified one line.

INSTALLATION
============

To compile this program you need Delphi 7 Professional or Entreprise, with Update 1
It may still work with Delphi 6 update 2 but FreeReport makes it quite unstable. Without Update 1 some help links will not work.

You will also need to download a package of components that I made from few components from myself, and some that I extracted from other packages :

http://update.antp.be/comps/antcomponents.zip

In addition of installing the package of these components, you will probably have to install the following components (except if you already have them, of course) :

- Toolbar 2000 + TBX : http://www.jrsoftware.org
- SynEdit 2 : http://synedit.sourceforge.net
- TPNGImage
- Indy 9 : http://www.indyproject.org/Sockets/Download/Files/Indy9.DE.aspx
- ElTree Lite 2.78 modified for Ant Movie Catalog 4.2.0 : an old version of TElTree Lite, from Eldos, which is not more available.
- HTML Viewer Components 9.45 : http://pbear.com/htmlviewers.html
- rkSmartView v1.0 modified for Ant Movie Catalog 4.2.0 : included in this package.
  (official version of rkSmartView can be found on http://rmklever.com)
- RegExpr v.0.952 by Andrey V. Sorokin (included directly in source code) : http://regexpstudio.com

All these components can be downloaded from there : http://update.antp.be/comps/

Also check http://www.antp.be/software/moviecatalog/sources/ for more info

You can contact me if you have problems to install/compile these files, or if you need some tips about the way AMC's code works
see http://www.antp.be/about/contact/
Please do not contact me if you are new to Delphi, try to find some tutorials first ;)
