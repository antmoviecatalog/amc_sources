Ant Movie Catalog 4.2.1

Copyright (C) 2000-2017 Antoine Potten, Micka�l Vanneufville

http://www.antp.be/software/moviecatalog

Install package updated on 2017-11-04

------------------------------------------------

ABOUT SCRIPTS UPDATES...

This installation package does not especially include latest available scripts for websites information importation. You can update the scripts by launching the script named "UPDATE SCRIPTS" (at the top of the list when importing information using scripts).
Alternatively, you can manually get scripts through http://update.antp.be/amc/scripts
When a script stops working, this is the first place to check, though that usually it takes a few days before an update is available.

IF YOU UPGRADE TO A NEW VERSION...

The installation process may delete or overwrite files from previous installations, which includes languages files, scripts, toolbar images, etc.
Configuration (xml and ini) and catalogues will not be affected.
If you added yourself a language file, a toolbar image, a script, etc. it will not be modified or delete by the installation/uninstallation.
If you modified files but are not sure if they are default files, I recommend to make a backup copy of the folder where you installed the program before upgrading to the new version.

Also, do not forget to OFTEN BACKUP YOUR CATALOGS; not only before upgrading to a new version.

------------------------------------------------

License information

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED, IMPLIED OR OTHERWISE, INCLUDING AND WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL THE AUTHOR OR HIS COMPANY BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, BUSINESS INTERRUPTION, LOSS OF INFORMATION, OR ANY OTHER LOSS) , WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR INABILITY TO USE THIS SOFTWARE.

